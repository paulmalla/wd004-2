const customer = {
	name: "Brandon Cruz",
	accountNo: "123-456",
	accountBal: 5000,

	showCustomer: function () {
	return "Customer Name: " + customer.name;
	},

	showAccountNumber: function () {
	return "Customer Account Number: " + customer.accountNo;
	},

	showCustomerDetails: function () {
	return "Customer Name: " + customer.name + ", Customer Account Number: " + customer.accountNo + ", Balance: " + customer.accountBal;
	},

	showBalance: function () {
	return "Account Balance: " + customer.accountBal;
	},

	deposit: function (amount) {
		return customer.accountBal += amount;
	},

	withdraw: function (amount) {
		return customer.accountBal -= amount;
	}
};


