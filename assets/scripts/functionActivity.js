
pi = 3.1416

function getSum (num1, num2) {
	const sum = num1 + num2;
	console.log(sum);
	return sum;
};

function getDifference (num1, num2) {
	const difference = num1 - num2;
	console.log(difference);
	return difference;
};

function getProduct (num1, num2) {
	const product = num1 * num2;
	console.log(product);
	return product;
};

function getQuotient (num1, num2) {
	const quotient = num1 / num2;
	console.log(quotient);
	return quotient;
};

//V=pi*r2*h
function getCylinderVolume (radius, height) {
	const rSquare = radius * radius;
	const rHeight = rSquare * height;
	const volume = pi * rHeight;
	console.log (volume);
	return volume;
};

//A=((a+b)/2)*h
function getTrapezoidArea (base1, base2, height) {
	const allBase = base1 + base2;
	const halfBase = allBase / 2;
	const area = halfBase * height;
	console.log (area);
	return area;
};

function getTotalAmount (cost, quantity, discount, tax) {
	const amount = cost * quantity;
	const aPercent = discount / 100;
	const aDiscount = amount * aPercent;
	const tAmount = amount - aDiscount;
	const totalAmount = tAmount + tax;
	console.log (totalAmount);
	return totalAmount;
};


function getTotalSalary (basic, days, absent, late, tax) {
	const salaryPerDay = basic / 20;
	const salaryPerHour = salaryPerDay / 8;
	const salaryPerMinute = salaryPerHour / 60;

	//absent
	const totalAbsent = absent * salaryPerDay;
	//const basicAbsent = basic - totalAbsent;

	//late
	const totalLate = late * salaryPerMinute;
	//const basicLate = basic - totalLate;

	//total deduction
	const totalDeduct = totalAbsent + totalLate;
	const taxDeduct = totalDeduct + tax;
	//const totalBasic = basic - totalDeduct;

	//salary
	const monthlySalary = basic - taxDeduct;

	//const aPercent = discount / 100;
	//const aDiscount = amount * aPercent;
	//const tAmount = amount - aDiscount;
	//const totalAmount = tAmount + tax;
	console.log (monthlySalary);
	return monthlySalary;
};

// basic 30000
// workingdays 20days
// workinghours 8hrs
// inminutes 480min (60mins per hr)

//1500 per day
//1 day = 480min
//per day / 8hrs / 60


